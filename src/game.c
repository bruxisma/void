#include "game.h"

#include <string.h>

g_shader_lib g_load_shader_lib(f_pointer pointer){
    g_shader_lib* lib = (g_shader_lib*)f_get_data(pointer);
    for(int i=0;i<lib->shader_count;++i){
      Shader s = r_create_shader_f(lib->shaders[i]);

    }
    return *lib;
}

g_audio_lib g_load_audio_lib(f_pointer pointer){
    g_audio_lib* lib = (g_audio_lib*)f_get_data(pointer);
    return *lib;
}

g_anim_lib g_load_anim_lib(f_pointer pointer){
  g_anim_lib* lib = (g_anim_lib*)f_get_data(pointer);
  return *lib;
}

g_level_info g_load_level_info(f_pointer pointer){
  g_level_info* lib = (g_level_info*)f_get_data(pointer);
  return *lib;
}
