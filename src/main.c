#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <time.h>
#include <unistd.h>
#include "timespec.h"

#include "types.h"
#include "tmath.h"

#define DEBUG_OUTPUT

#ifndef DEBUG_OUTPUT
#undef  _WIN32_WINNT
#define _WIN32_WINNT 0x0500
#endif

#include "input.h"

#define STB_VORBIS_HEADER_ONLY
#include "audio.h"

#define STB_IMAGE_IMPLEMENTATION
#include "render.h"


#include "user.h"

typedef struct timespec tms;

int targetFPS = 60;
int maxTargetFPS = 120;

static r_window window;

static r_tex default_tex;
static r_shader  default_shader;
static r_quad    default_quad;
static r_camera  default_cam;

a_buffer test_sound;
a_music* music;

static r_tex sheet_tex;
static r_texsheet sheet;

static mat4 model;

static int tex_id = 0;
static int max_tex_id = 4;

int fsign(float x){
    return (int)((x > 0) - (x < 0));
}

void input(double delta){
    if(i_key_clicked(GLFW_KEY_ESCAPE)){
        r_request_close();
    }

    float vert = i_opposing("forward", "back") * -1;
    if(vert != 0){
        default_cam.pos.y += fsign(vert) * 6.f * delta * 0.001f;
    }

    float horiz = i_opposing("right", "left") * -1;
    if(horiz != 0){
        default_cam.pos.x += fsign(horiz) * 6.f * delta * 0.001f;
    }

    if(horiz != 0 || vert != 0){
        r_update_camera(&default_cam);
    }

    if(i_key_clicked('E')){
        tex_id ++;
        if(tex_id == max_tex_id) tex_id = 0;
    }

    if(i_key_clicked('Q')){
        tex_id --;
        if(tex_id < 0) tex_id = max_tex_id;
    }

    if(i_key_clicked('G')){
        printf("GGGGG\n");
    }

}

void init_render(){
    bool r = r_no_err();
    if(!r) printf("Window Creation.\n");

    default_tex = r_load_tex("res/textures/icon.png");
    r = r_no_err();
    if(!r) printf("Texture Loading\n");

    default_shader = r_create_shader("res/shaders/main.vert", "res/shaders/main.frag");
    r = r_no_err();
    if(!r) printf("Shader Loading.\n");

    default_quad = r_create_quad(1.f, 1.f);
    r = r_no_err();
    if(!r) printf("Quad creation.\n");

    default_cam.size = (vec2){ 16.f, 9.f };
    r_create_camera(&default_cam);

    sheet_tex = r_load_tex("res/textures/texturesheet.png");
    sheet = r_create_tex_sheet(&sheet_tex, 16, 16);

    r_loop_err();

    mat4_identity(&model);
    mat4_translate(&model, 0.f, 0.f, -1.f);
}

void init_audio(){
    if(!a_load_devices()){
        printf("Unable to populate audio devices\n");
        return;
    }

    if(!a_create_context(NULL)){
        printf("Unable to create audio context.\n");
        return;
    }

    music = a_create_music("res/audio/bg.ogg");
    music->loop = true;
}

void exit_audio(){
#ifdef CACHE_AUDIO_FILES
    a_destroy_file_cache();
#endif
    a_destroy_context();
}

void update(long delta){
    a_update_sfx();
    a_update_music(music);
}

void render(){
    r_bind_shader(&default_shader);
    r_set_uniformi(default_shader, "sub_texture", 1);

    r_set_mat4(default_shader, "projection", default_cam.proj);
    r_set_mat4(default_shader, "view", default_cam.view);
    r_set_mat4(default_shader, "model", model);

    r_set_uniformi(default_shader, "color_mode", -1);

    r_set_vec2(default_shader, "sub_size", (vec2){16.f, 16.f});
    r_set_vec2(default_shader, "tex_size", (vec2){sheet_tex.width, sheet_tex.height});
    r_set_uniformi(default_shader, "tex_id", tex_id);

    r_bind_tex(sheet_tex);
    r_draw_quad(&default_quad);
}

void test_toml(){
    u_toml_conf conf = u_load_conf((f_pointer){"res/conf/test.toml"});
    u_toml_conf general = u_get_sub_conf(conf, "general");

    char* name;
    u_gets(general, "name", &name);
    int len = strlen(name);
    name[len] = 0;

    long xp = u_getl(general, "xp");
    printf("%li\n", xp);
    printf("%s\n", name);
    u_clean_conf(conf);
}

int main(int argc, char** argv){
#ifdef __MINGW32__
#ifndef DEBUG_OUTPUT
    FreeConsole();
#endif
#endif
    test_toml();

    double timeframe = MS_PER_SEC / (double)targetFPS;
    double current = time_get_time();
    double last = current;
    double check;

    double delta;
    double accum = timeframe;

    r_window_info window_info = (r_window_info){
        1280, 720,
        false, false, false,
        60
    };

    r_create_window(window_info);

    init_render();
    init_audio();

    while(!r_window_should_close()){
        last = current;
        current = time_get_time();
        delta = current - last;
        accum = timeframe;

        r_clear_window();
        render();
        r_swap_buffers();

        i_update();
        glfwPollEvents();
        input(delta);

        update(delta);

        check = time_get_time();
        accum = (long)(check - current);

        double nTimeFrame = timeframe;
        int tFPS;
        int lastFPS = targetFPS;

        if(accum > 0){
            nTimeFrame -= accum;
            tFPS = (int)((double)MS_PER_SEC / (nTimeFrame));

            if(tFPS > maxTargetFPS){
                targetFPS = maxTargetFPS;
            }else if(tFPS > 0){
                targetFPS = tFPS;
            }

            timeframe = (double)(MS_PER_SEC / (double)(targetFPS));

            tms sleepreq, sleeprem;
            sleepreq.tv_sec = 0;
            sleepreq.tv_nsec = accum * NS_PER_MS;

            nanosleep(&sleepreq, &sleeprem);
        }else{
            nTimeFrame += accum;

            tFPS = (int)((double)MS_PER_SEC / (nTimeFrame));

            if(tFPS < maxTargetFPS){
                targetFPS = maxTargetFPS;
            }else if(tFPS < 0){
                targetFPS = 1;
            }else{
                targetFPS = tFPS;
            }

            timeframe = (double)(MS_PER_SEC / (double)(targetFPS));
        }
    }

    exit_audio();

    r_destroy_shader(&default_shader);
    r_destroy_tex(&default_tex);
    r_destroy_quad(&default_quad);
    r_destroy_window(&window);

    glfwTerminate();
    return EXIT_SUCCESS;
}
