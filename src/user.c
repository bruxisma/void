#include "user.h"

#include <string.h>
#include <stdio.h>

u_toml_conf u_load_conf(f_pointer p){
    u_toml_conf conf;

    FILE* f = fopen(p.path, "r");

    if(!f){
        printf("Unable to open toml file: %s\n", p.path);
        return conf;
    }

    char err_buf[128];
    conf.table = toml_parse_file(f, err_buf, 128 * sizeof(char));
    fclose(f);

    if(conf.table == 0){
        printf("TOML: %s\n", err_buf);
    }

    conf.file = p;
    conf.is_sub = 0;

    return conf;
}

double u_getd(u_toml_conf conf, const char* value){
    const char* raw;
    double res;
    raw = toml_raw_in(conf.table, value);
    toml_rtod(raw, &res);
    return res;
}

int u_getslen(u_toml_conf conf, const char* value){
    const char* raw;
    char* str;

    raw = toml_raw_in(conf.table, value);

    if(toml_rtos(raw, &str) == -1){
        printf("toml unable to convert string.\n");
        return 0;
    }

    int len = strlen(str);
    free(str);

    return len;
}

void u_gets(u_toml_conf conf, const char* value, char** ptr){
    const char* raw;

    raw = toml_raw_in(conf.table, value);
    if(toml_rtos(raw, ptr)){
        printf("toml unable to convert string\n");
    }
}

long u_getl(u_toml_conf conf, const char* value){
    const char* raw = toml_raw_in(conf.table, value);
    long res;

    if(toml_rtoi(raw, &res) == -1){
        printf("toml unable to convert long\n");
        return 0;
    }

    return res;
}

u_toml_conf u_get_sub_conf(u_toml_conf conf, const char* name){
    u_toml_conf table;

    table.table = toml_table_in(conf.table, name);

    if(table.table == 0){
        printf("Unable to get table: %s\n", name);
        return (u_toml_conf){0};       
    }

    table.file = conf.file;
    table.is_sub = 1;
    return table;
}

void  u_clean_conf(u_toml_conf conf){
    if(conf.is_sub){
        return;
    }

    toml_free(conf.table);
}

