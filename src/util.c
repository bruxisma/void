#include "util.h"

#include <string.h>

int util_hex_number(char v){
    if(v >= '0' && v <= '9'){
        return v - 0x30;
    }else{
        switch(v){
            case 'A': case 'a': return 10;
            case 'B': case 'b': return 11;
            case 'C': case 'c': return 12;
            case 'D': case 'd': return 13;
            case 'E': case 'e': return 14;
            case 'F': case 'f': return 15;
            default: return 0;
        }
    }
}

int util_hex_multi(char* v, int len){
    if(len == 2){
        return util_hex_number(v[0])*16+util_hex_number(v[1]);
    }else if(len == 1){
        return util_hex_number(v[0])*16+util_hex_number(v[0]);
    }
}

float util_hex_gl_color(char* v){
    return (util_hex_multi(v, strlen(v))) / 255.f;
}

vec3 util_get_color(char* v){
    int len = strlen(v);
    int offset = 0;
    if(len == 4){
        offset = 1;
        len = 3;
    }else if(len == 7){
        offset = 1;
        len = 6;
    }

    vec3 val = {0.f};

    if(len == 3){
        val.x = util_hex_multi(&v[offset], 1)   / 255.f;
        val.y = util_hex_multi(&v[offset+1], 1) / 255.f;
        val.z = util_hex_multi(&v[offset+2], 1) / 255.f;
    }else if(len == 6){
        val.x = util_hex_multi(&v[offset], 2)   / 255.f;
        val.y = util_hex_multi(&v[offset+2], 2) / 255.f;
        val.z = util_hex_multi(&v[offset+4], 2) / 255.f;
    }

    return val;
}
