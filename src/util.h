#ifndef UTIL_H
#define UTIL_H
#include "types.h"

int   util_hex_multi(char* hex, int len);
int   util_hex_number(char v);
float util_hex_gl_color(char* hex);
vec3  util_get_color(char* hex);

#endif
